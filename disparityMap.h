#include "Header.h"

Mat computeDisparityMap(string path_img1, string path_img2)
{
	Mat img1 = imread(path_img1,CV_LOAD_IMAGE_COLOR);
	Mat img2 = imread(path_img2,CV_LOAD_IMAGE_COLOR);

	if(img1.empty() || img2.empty())
	{
		cout << "\ncheck  input images\n";
		return Mat();
	}


	// Stereo var
	Mat sv_disp;
	StereoVar sv;

	sv(img1, img2, sv_disp);

	/*

	// SBM
	StereoBM sbm;
	sbm.state->SADWindowSize = 9;
	sbm.state->numberOfDisparities = (((img1.cols/8) + 15) & -16); //112;
	sbm.state->preFilterSize = 5;
	sbm.state->preFilterCap = 61;
	sbm.state->minDisparity = -39;
	sbm.state->textureThreshold = 507;
	sbm.state->uniquenessRatio = 0;
	sbm.state->speckleWindowSize = 0;
	sbm.state->speckleRange = 8;
	sbm.state->disp12MaxDiff = 1;

	
	Mat gray_img1,gray_img2;
	cvtColor(img1, gray_img1, CV_RGB2GRAY);
	cvtColor(img2, gray_img2, CV_RGB2GRAY);

	Mat disp, disp8;

	sbm(gray_img1, gray_img2, disp);

	normalize(disp, disp8, 0, 255, CV_MINMAX, CV_8U);
	*/
	return sv_disp;
}


