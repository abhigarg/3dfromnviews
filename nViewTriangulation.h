#include "Header.h"


struct triangulateResult{
	vector<Point3f> pt_3d;
	vector<double> reproject_error;
};

	//opencv triangulation
triangulateResult triangulateOpenCV(vector<vector<Point2f>> img_pair_pts, Mat K, Mat distCoeffs, Matx34d P1, Matx34d P2)
{
	//undistort
	Mat pt1,pt2;
	undistortPoints(img_pair_pts[0], pt1, K, distCoeffs);
	undistortPoints(img_pair_pts[1], pt2, K, distCoeffs);
	
	//triangulate
	Mat pt_set1_pt_2r = pt1.reshape(1, 2);
	Mat pt_set2_pt_2r = pt2.reshape(1, 2);

	unsigned int pts_size = img_pair_pts[0].size();

	Mat pt_3d_h(1, pts_size,CV_32FC4);
	triangulatePoints(P1,P2,pt_set1_pt_2r,pt_set2_pt_2r,pt_3d_h);
	cout << "\npoints trinagulated successfully..\n";

	//calculate reprojection
	vector<Point3f> pt_3d;
	convertPointsHomogeneous(pt_3d_h.reshape(4, 1), pt_3d);
	Mat_<double> R = (Mat_<double>(3,3) << P1(0,0),P1(0,1),P1(0,2), P1(1,0),P1(1,1),P1(1,2), P1(2,0),P1(2,1),P1(2,2));
	Vec3d rvec; Rodrigues(R ,rvec);
	Vec3d tvec(P1(0,3),P1(1,3),P1(2,3));
	vector<Point2f> reprojected_pt_set1;
	projectPoints(pt_3d,rvec,tvec,K,distCoeffs,reprojected_pt_set1);

	vector<double> reproj_error;
	for (unsigned int i=0; i<pts_size; i++)
		reproj_error.push_back(norm(img_pair_pts[0][i]-reprojected_pt_set1[i]));

	triangulateResult trResult;
	trResult.pt_3d = pt_3d;
	trResult.reproject_error = reproj_error;

	return trResult;

}


//************************************************************************************************************************************/
									//Triangulation using Hartley and Zisserman Method//
//*************************************************************************************************************************************/

/**
 From "Triangulation", Hartley, R.I. and Sturm, P., Computer vision and image understanding, 1997
 */
Mat_<double> LinearLSTriangulation(Point3d u,		//homogenous image point (u,v,1)
								   Matx34d P,		//camera 1 matrix
								   Point3d u1,		//homogenous image point in 2nd camera
								   Matx34d P1		//camera 2 matrix
								   ) 
{
	
	//build matrix A for homogenous equation system Ax = 0
	//assume X = (x,y,z,1), for Linear-LS method
	//which turns it into a AX = B system, where A is 4x3, X is 3x1 and B is 4x1
	//	cout << "u " << u <<", u1 " << u1 << endl;
	//	Matx<double,6,4> A; //this is for the AX=0 case, and with linear dependence..
	//	A(0) = u.x*P(2)-P(0);
	//	A(1) = u.y*P(2)-P(1);
	//	A(2) = u.x*P(1)-u.y*P(0);
	//	A(3) = u1.x*P1(2)-P1(0);
	//	A(4) = u1.y*P1(2)-P1(1);
	//	A(5) = u1.x*P(1)-u1.y*P1(0);
	//	Matx43d A; //not working for some reason...
	//	A(0) = u.x*P(2)-P(0);
	//	A(1) = u.y*P(2)-P(1);
	//	A(2) = u1.x*P1(2)-P1(0);
	//	A(3) = u1.y*P1(2)-P1(1);
	Matx43d A(u.x*P(2,0)-P(0,0),	u.x*P(2,1)-P(0,1),		u.x*P(2,2)-P(0,2),		
			  u.y*P(2,0)-P(1,0),	u.y*P(2,1)-P(1,1),		u.y*P(2,2)-P(1,2),		
			  u1.x*P1(2,0)-P1(0,0), u1.x*P1(2,1)-P1(0,1),	u1.x*P1(2,2)-P1(0,2),	
			  u1.y*P1(2,0)-P1(1,0), u1.y*P1(2,1)-P1(1,1),	u1.y*P1(2,2)-P1(1,2)
			  );
	Matx41d B(-(u.x*P(2,3)	-P(0,3)),
			  -(u.y*P(2,3)	-P(1,3)),
			  -(u1.x*P1(2,3)	-P1(0,3)),
			  -(u1.y*P1(2,3)	-P1(1,3)));
	
	Mat_<double> X;
	solve(A,B,X,DECOMP_SVD);
	
	return X;
}


/**
 From "Triangulation", Hartley, R.I. and Sturm, P., Computer vision and image understanding, 1997
 */
Mat_<double> IterativeLinearLSTriangulation(Point3d u,	//homogenous image point (u,v,1)
											Matx34d P,			//camera 1 matrix
											Point3d u1,			//homogenous image point in 2nd camera
											Matx34d P1			//camera 2 matrix
											) {
	#define EPSILON 0.000001
	double wi = 1, wi1 = 1;
	Mat_<double> X(4,1);
	Mat_<double> X_ = LinearLSTriangulation(u,P,u1,P1);
	X(0) = X_(0); X(1) = X_(1); X(2) = X_(2); X(3) = 1.0;
		
	//calculate weights
	double p2x = Mat_<double>(Mat_<double>(P).row(2)*X)(0);
	double p2x1 = Mat_<double>(Mat_<double>(P1).row(2)*X)(0);
	
	wi = p2x;
	wi1 = p2x1;

	for (int i=0; i<10; i++) { //Hartley suggests 10 iterations at most
		//reweight equations and solve

		cout << "\nIteration no " << i+1 << " for LST\n" << endl;

		Matx43d A((u.x*P(2,0)-P(0,0))/wi,		(u.x*P(2,1)-P(0,1))/wi,			(u.x*P(2,2)-P(0,2))/wi,		
				  (u.y*P(2,0)-P(1,0))/wi,		(u.y*P(2,1)-P(1,1))/wi,			(u.y*P(2,2)-P(1,2))/wi,		
				  (u1.x*P1(2,0)-P1(0,0))/wi1,	(u1.x*P1(2,1)-P1(0,1))/wi1,		(u1.x*P1(2,2)-P1(0,2))/wi1,	
				  (u1.y*P1(2,0)-P1(1,0))/wi1,	(u1.y*P1(2,1)-P1(1,1))/wi1,		(u1.y*P1(2,2)-P1(1,2))/wi1
				  );
		Mat_<double> B = (Mat_<double>(4,1) <<	  -(u.x*P(2,3)	-P(0,3))/wi,
												  -(u.y*P(2,3)	-P(1,3))/wi,
												  -(u1.x*P1(2,3)	-P1(0,3))/wi1,
												  -(u1.y*P1(2,3)	-P1(1,3))/wi1
						  );
		
		solve(A,B,X_,DECOMP_SVD);
		X(0) = X_(0); X(1) = X_(1); X(2) = X_(2); X(3) = 1.0;

		//recalculate weights
		double p2x = Mat_<double>(Mat_<double>(P).row(2)*X)(0);
		double p2x1 = Mat_<double>(Mat_<double>(P1).row(2)*X)(0);

		//breaking point
		if(fabsf(wi - p2x) <= EPSILON && fabsf(wi1 - p2x1) <= EPSILON) break;
			
		wi = p2x;
		wi1 = p2x1;
	}
	return X;
}


bool testTriangulation(const vector<Point3f>& pcloud_pt3d, const Matx34d& P, vector<uchar>& status) {
	//vector<Point3d> pcloud_pt3d = CloudPointsToPoints(pcloud);
	vector<Point3f> pcloud_pt3d_projected(pcloud_pt3d.size());
	
	Matx44d P4x4 = Matx44d::eye(); 
	for(int i=0;i<12;i++) P4x4.val[i] = P.val[i];
	
	perspectiveTransform(pcloud_pt3d, pcloud_pt3d_projected, P4x4);
	
	status.resize(pcloud_pt3d.size(),0);
	for (int i=0; i<pcloud_pt3d.size(); i++) 
	{
		status[i] = (pcloud_pt3d_projected[i].z > 0) ? 1 : 0;
	}
	int count = countNonZero(status);

	double percentage = ((double)count / (double)pcloud_pt3d.size());
	cout << count << "/" << pcloud_pt3d.size() << " = " << percentage*100.0 << "% are in front of camera" << endl;
	if(percentage < 0.75)
	{
		cout << "\nless than 75% of the points are in front of the camera" << endl;
		return false; 
	}
	
	//check for coplanarity of points
	else //not
	{
		if(pcloud_pt3d.size() >= 3)
		{
			cout << "\nChecking for coplanarity of points:" << endl;
			Mat_<double> cldm(pcloud_pt3d.size(),3);
			for(unsigned int i=0;i<pcloud_pt3d.size();i++) 
			{
				cldm.row(i)(0) = pcloud_pt3d[i].x;
				cldm.row(i)(1) = pcloud_pt3d[i].y;
				cldm.row(i)(2) = pcloud_pt3d[i].z;
			}
			Mat_<double> mean;
			PCA pca(cldm,mean, CV_PCA_DATA_AS_ROW);

			int num_inliers = 0;
			Vec3f nrm = pca.eigenvectors.row(2); nrm = nrm / norm(nrm);
			Vec3f x0 = pca.mean;
			double p_to_plane_thresh = sqrt(pca.eigenvalues.at<double>(2));

			for (int i=0; i<pcloud_pt3d.size(); i++) 
			{
				Vec3f w = Vec3f(pcloud_pt3d[i]) - x0;
				double D = fabs(nrm.dot(w));
				if(D < p_to_plane_thresh) num_inliers++;
			}

			//cout << num_inliers << "/" << pcloud_pt3d.size() << " are coplanar" << endl;
			if((double)num_inliers / (double)(pcloud_pt3d.size()) > 0.85)
			{
				cout << "\n points are coplanar" << endl;
				return false;
			}
		}
	}

	return true;
}



triangulateResult triangulateHnZ(const vector<Point2f>& points1, const vector<Point2f>& points2, const Mat& K, 
								 const Mat& cDistCoeffs, const Matx34d& P1, const Matx34d& P2)
{
	vector<Point3f> estimated3d;   // to store estimated 3d coordinates for different possible camera projection matrices
	vector<double> reproj_error;

	cout << "\nEstimated 3D coordinates by Triangulation HnZ: " << "\n" << endl;

	//http://www.morethantechnical.com/2012/01/04/simple-triangulation-with-opencv-from-harley-zisserman-w-code/
	for(int i = 0; i < points1.size(); i++)   // for all visible image point-pairs
	{	
		Mat Kinv = K.inv();
		// homogeneous image coordinates
		Point3d u1(points1[i].x, points1[i].y, 1.0);
		Mat_<double> um = Kinv * Mat_<double>(u1); // normalize 
		u1.x = um(0); u1.y = um(1); u1.z = um(2);
			
		Point3d u2(points2[i].x, points2[i].y, 1.0);
		um = Kinv * Mat_<double>(u2); 
		u2.x = um(0); u2.y = um(1); u2.z = um(2);
	
		Mat_<double> X = IterativeLinearLSTriangulation(u1, P1, u2, P2);
		Mat_<double> xPt_img = K * Mat(P1) * X;  // reproject

		Point2f xPt_img_(xPt_img(0)/xPt_img(2),xPt_img(1)/xPt_img(2));
		estimated3d.push_back(Point3f(X(0), X(1), X(2)));

		cout << "\nX: " << X(0) << "Y: " << X(1) << "Z: " << X(2) << "\n" << endl;


		double reprj_err = norm(xPt_img_-points1[i]);
		reproj_error.push_back(reprj_err);
	}

	//Scalar mse = mean(reproj_error);

	triangulateResult trResult;
	trResult.pt_3d = estimated3d;
	trResult.reproject_error = reproj_error;//mse[0];

	return trResult;
}