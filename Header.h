// System Includes
# include <stdio.h>
# include <stdlib.h>
# include <tchar.h>
# include <iostream>
# include <fstream>
# include <io.h>
# include <string>

// OpenCV Includes
# include <opencv2\imgproc\imgproc.hpp>
# include <opencv2\highgui\highgui.hpp>
# include <opencv2\core\core.hpp>
# include <opencv2\contrib\contrib.hpp>
# include <opencv2\calib3d\calib3d.hpp>
# include "opencv2/features2d/features2d.hpp"
# include <opencv2/nonfree/features2d.hpp>
# include <opencv\cv.h>
# include <opencv\highgui.h>
# include <opencv\cvaux.h>
# include <opencv\cxcore.h>

#include <time.h>

using namespace cv;
using namespace std;


