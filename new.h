#include "Header.h"

int new_imshow(string window_name, Mat img, string save_name)
{
	imshow(window_name, img);

	char ky = waitKey(0);

	if(ky == 'q')
	{
	//	destroyWindow(window_name);
		return 0;
	}

	if(ky =='s' && !save_name.empty())
	{
		imwrite(save_name, img);
//		destroyWindow(window_name);
		return 0;
	}


	return 1;
}