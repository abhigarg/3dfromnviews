// opticFlow.cpp : Defines the entry point for the console application.
//


//#include "stdafx.h"

//#include "opticFlow.h"
#include "findCameraMatrix.h"
#include "nViewTriangulation.h"
#include "disparityMap.h"
#include "new.h"


// Error codes:

// -1: Not enough input arguments
// -2: camera calibration input file not found
// -3: check  input images (not found or cant read by opencv), disparity image can not find
// -4: Not enough matching features found
// -5: no keypoints found for image 1
// -6: no keypoints found for image 2
// -7: Failed to open keypts & descriptors yml file (if exists)
// -8: Not enough good features found
// -9: camera projection matrix not found
// -10: Triangulation not successful 
//

int main(int argc, char** argv)   //usuage: sfm.exe <path_img1> <path_img2> <camera_calibration.txt> <output.txt>
{
	ofstream log;
	log.open("log.txt");

	if(argc < 5)
	{
		log << "Not enough input arguments\n";
		log.close();
		return -1;
	}

	bool show_img = false;

	if(argc == 6)
		bool show_img = argv[5];

	string path_img1 = argv[1]; 
	string path_img2 = argv[2]; 


	string fname_img,output_dir;
	size_t dot = path_img1.find_last_of(".");
	size_t slash = path_img1.find_last_of("\\");

	if (dot != std::string::npos)
        fname_img = path_img1.substr(0, dot);

	if (slash != std::string::npos)
        output_dir = fname_img.substr(0, slash);

	output_dir.append("\\Data");

	log << "image path: " << fname_img << "\n\n";

	log << "output dir: " << output_dir << "\n\n";
	
	string path_camcalib = argv[3];
	/*
	// read camera matrix and distortion coefficients from text file
	ifstream camcalib_file; 
	camcalib_file.open( path_camcalib, ios_base::in );
	*/

	string path_output = argv[4];

	// output text file
	ofstream output_txtFile;
	output_txtFile.open(path_output);
	/*
	if(!camcalib_file.is_open())
	{
		log << "\ncamera calibration input file not found\n";
		log.close();
		return -2;
	}
	*/
	string line;
	//string number;
	vector<double> data;

	double num;
	/*
	while(!camcalib_file.eof())
	{           
		getline(camcalib_file,line); //read number
		istringstream in(line);
		in >> num;
        data.push_back(num); //convert to integer
    }
	*/
	/*
	// disparity map
	Mat disparity_map;
	disparity_map = computeDisparityMap(path_img1, path_img2);
	
	if(disparity_map.empty())
	{
		log << "\ndisparity map not found ... check input images\n" << '\n';
		log.close();
		return -3;
	}
	
	if(show_img)
	{
		imshow("Disparity Map", disparity_map);
		waitKey(2);
	}
	*/
	
	// camera matrix
	Mat K = Mat::zeros(3,3,CV_64FC1);
	K.at<double>(0, 0) = 2134.3887; //(float)data[0];      //fx
	K.at<double>(1, 1) = 2141.0983;  //(float)data[1];      //fy
	K.at<double>(0, 2) = 3003.0506;//(float)data[2];      //cx 
	K.at<double>(1, 2) = 1969.2121;  //(float)data[3];      //cy
	K.at<double>(2, 2) = 1;

	//distortion coefficients
	Mat	distCoeffs = Mat::zeros( 1, 4, CV_64FC1 );
	distCoeffs.at<double>(0,0) = 0.0374;//(float)data[4];
	distCoeffs.at<double>(0,1) = 0.0068;//(float)data[5];
	distCoeffs.at<double>(0,2) = -0.0005;//(float)data[6]; 
	distCoeffs.at<double>(0,3) = 0.0004;//(float)data[7]; 
	
	

	// projection matrices
	vector<Matx34d> P;
	vector<Matx34d> P_;

	// define 1st cam as origin
	Matx34d P0(1,0,0,0,
			   0,1,0,0,
			   0,0,1,0);

	P.push_back(P0);
	
	//projection matrices for next series of cameras will be obtained using triangulation between pair of cams

	//feature matching
	vector<vector<Point2f>> img_pair_pts;
	vector<vector<Point2f>> undist_img_pair_pts;
	double thresh1 = 0.6;
	int thresh2 = 3;
	if(!findMatchingPoints(path_img1, path_img2, img_pair_pts, thresh1, thresh2, show_img))
	{
		log << "\nmatches features not found .. \n";
		log.close();
		return -4;
	}

	log << "\nmatching features found..\n";

	log << "\nfinding projection matrix for cam-2 ... \n";
	
	// fundamental matrix
	Mat F( 3, 3, CV_64FC1 );

	log << "\n total no of common points: " << img_pair_pts[0].size() << "\n";

	//Undistort common points:
	vector<Point2f> undist1;
	vector<Point2f> undist2;
	undistortPoints(img_pair_pts[0], undist1, K, distCoeffs);
	undistortPoints(img_pair_pts[1], undist2, K, distCoeffs);

	undist_img_pair_pts.push_back(undist1);
	undist_img_pair_pts.push_back(undist2);

	if(img_pair_pts[1].size() > 20)
		F = findFundamentalMat( undist1, undist2, CV_FM_RANSAC);
	else
		F = findFundamentalMat( undist1, undist2, CV_FM_8POINT);

	log << "\n\nFundamental Matrix F:\n\n" << F << "\n" << endl;
	
	if(!findCamProjMatrix(F, K, P_))
	{
		log << "\ncamera projection matrix not found ...\n";
		log.close();
		return -9;
	}
	
	log << "\cam2 projection matrix found .. \n";

	unsigned int pts_size = img_pair_pts[0].size();

	// trinagulation using HnZ
	vector<Point3f> pt_3d;
	vector<uchar> tmp_status;
	Scalar mse;
	bool tr_sucessful = false;
	double reproject_error;

	// Find the appropriate P2 = [R|t] among solutions found by Decomposition of Essential Matrix by triangulating using Harley & Zisserman method
	//http://www.morethantechnical.com/2012/01/04/simple-triangulation-with-opencv-from-harley-zisserman-w-code/
	for (int i = 0; i < P_.size(); i++)
	{
		triangulateResult trResult1 = triangulateHnZ(undist_img_pair_pts[0], undist_img_pair_pts[1], K, distCoeffs, P[0], P_[i]);
		triangulateResult trResult2 = triangulateHnZ(undist_img_pair_pts[1], undist_img_pair_pts[0], K, distCoeffs, P_[i], P[0]);
		
		vector<Point3f> pt1_3d, pt2_3d;
		double reproject_error1, reproject_error2;

		for(int j=0;j<trResult1.pt_3d.size(); j++)
			pt1_3d.push_back(trResult1.pt_3d[j]);
		mse =  mean(trResult1.reproject_error);
		reproject_error1 = mse[0];
		log << "reprojection error-1:" << reproject_error1 << endl;

		for(int j=0;j<trResult2.pt_3d.size(); j++)
			pt2_3d.push_back(trResult2.pt_3d[j]);

		mse =  mean(trResult2.reproject_error);
		reproject_error2 = mse[0];
		log << "reprojection error-2:" << reproject_error2 << endl;

		//check if points are triangulated --in front-- of cameras for at least 50% of points with re-projection error of < 100
		if (testTriangulation(pt1_3d, P[0], tmp_status) && testTriangulation(pt2_3d, P_[i],tmp_status) && reproject_error1 < 100.0 && reproject_error2 < 100.0)
		{
			log << i << "th matrix selected as the 3D transformation matrix" << endl;

			tr_sucessful = true;
			pt_3d = pt1_3d;
			mse = mean(trResult1.reproject_error);
			P.push_back(P_[i]);
			log<< "\nreprojection error: " << mse[0] << "\n";
			output_txtFile << "Triangulation result:\n";
			output_txtFile << "\nreprojection error: " << mse[0] << "\n";
			log << "\nTriangulation successful for pair of images" << endl;
			break;
		}
	}

	if (!tr_sucessful)
	{
		log << "\nTriangulation not successful for pair of input images by Fundamental Matrix approach" << endl;
		log.close();
		return -10;
	}


	Mat_<double> R(3,3);
	Mat_<double> T(3,1);
	Mat R1, R2, P1, P2, Q;

	
	for(int i = 0; i < 3; i++)
		for(int j = 0; j < 3; j++)
			R.at<double>(i,j) = P[1](i,j);

	output_txtFile << "\nRotation matrix found: \n\n" << R << "\n" << endl;
	log << "\nRotation matrix found: \n\n" << R << "\n" << endl;

	T.at<double>(0,0) = P[1](0,3);
	T.at<double>(1,0) = P[1](1,3);
	T.at<double>(2,0) = P[1](2,3);

	output_txtFile << "\nTranslation matrix found: \n\n" << T << "\n" << endl;
	log << "\nTranslation matrix found: \n\n" << T << "\n" << endl;

	/*
	Mat img = imread(path_img1);
	Rect roi1, roi2;

	stereoRectify(K, distCoeffs, K, distCoeffs, Size(img.size()), R, T, R1, R2, P1, P2, Q, CV_CALIB_ZERO_DISPARITY, -1, Size(img.size()), &roi1, &roi2);
	
	output_txtFile << "\nMatrix Q found:\n\n" << Q << "\n\n" << endl;  
	output_txtFile.close();
	log << "\nMatrix Q found:\n\n" << Q << "\n\n" << endl; 

	log << "Reprojecting disparity image to 3d .. \n" << endl;

	Mat _3dImg, dem, dem2, dem_fc, dem_mask;

	vector<Point3f> dem_xyz;

	vector<Mat> channel;

	reprojectImageTo3D(disparity_map, _3dImg, Q,true, -1);
	
	log << "\nSize of 3D image: " << _3dImg.size() << "\n" << endl;

	split(_3dImg, channel);
	dem = Mat_<float>(channel[2]);
	
	applyColorMap(dem, dem_fc, COLORMAP_HOT);

	threshold(dem, dem_mask, 9999, 255, THRESH_BINARY_INV);

	double minVal, maxVal;
	dem_mask.convertTo(dem_mask, CV_8UC1);

	minMaxIdx(dem, &minVal, &maxVal, 0, 0, dem_mask);

	Mat mean_dem, std_dem;
	meanStdDev(dem, mean_dem, std_dem, dem_mask);

	log << "\n min Z: " << minVal << "\n";
	log << "\n max Z: " << maxVal << "\n";
	log << "\n mean Z: " << mean_dem << "\n";
	log << "\n std dev Z: " << std_dem << "\n"; 

	dem2 = Mat::zeros(dem.size(), CV_8UC1);

	normalize(dem, dem2, 10, 200, NORM_MINMAX, CV_8UC1, dem_mask);

	applyColorMap(dem2, dem_fc, COLORMAP_JET);

	imshow("DEM", dem2);
	
	string dem_str;
	dem_str = output_dir;
	dem_str.append("\\DEM.jpg");
	imwrite(dem_str, dem2); 
	
	imshow("DEM FC", dem_fc);
	
	string demfc_str;
	demfc_str = output_dir;
	demfc_str.append("\\DEM_FC.jpg");
	imwrite(demfc_str,dem_fc);
	
	string disp_str;
	disp_str = output_dir;
	disp_str.append("\\DISP.jpg");
	imwrite(dem_str, disparity_map);
	waitKey(5);

	log << "\nprogram ran successfully\n";
	*/
	return 1;
}



/*
// camera matrix
	Mat K = Mat::zeros(3,3,CV_64FC1);
	K.at<double>(0, 0) = 246.00559;
	K.at<double>(1, 1) = 247.37317;
	K.at<double>(0, 2) = 169.87374;
	K.at<double>(1, 2) = 132.21396;
	K.at<double>(2, 2) = 1;

	//distortion coefficients
	Mat	distCoeffs = Mat::zeros( 1, 5, CV_64FC1 );
	distCoeffs.at<double>(0,1) = 0.04674;
	distCoeffs.at<double>(0,2) = -0.11775; 
	distCoeffs.at<double>(0,3) = -0.00464; 
	distCoeffs.at<double>(0,4) = -0.00346;


	C:\Users\Conrad\Documents\Visual Studio 2012\Projects\sfm\x64\Release>sfm.exe C:
\Users\Conrad\Dropbox\Conrad Personal\PhD\DataSets\SEEKER\im10070.jpg C:\Users\C
onrad\Dropbox\Conrad Personal\PhD\DataSets\SEEKER\im10086.jpg seeker_calibration
.txt output.txt

*/