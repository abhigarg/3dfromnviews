#include "Header.h"


void CheckNNDR(const vector<vector<DMatch> > &match_12, vector<vector<DMatch> > &match_21, vector<DMatch> &match_sym, double thresh )
{
    double ratio12, ratio21;
    ratio12 = 0;
    ratio21 = 0;
	
	// Select the most matching features. A lower threshold may miss some features
    //thresh = 0.7;  // threshold for NNDR
    
    for( size_t fwd_ind = 0; fwd_ind < match_12.size(); fwd_ind++ )
    {
        if(match_12[fwd_ind][0].distance < match_12[fwd_ind][1].distance)
            ratio12 = match_12[fwd_ind][0].distance / match_12[fwd_ind][1].distance;

        if(match_12[fwd_ind][0].distance > match_12[fwd_ind][1].distance)
            ratio12 = match_12[fwd_ind][1].distance / match_12[fwd_ind][0].distance;
        
		if(ratio12 <= thresh)
        {
            for( size_t fwd_match_ind = 0; fwd_match_ind < match_12[fwd_ind].size(); fwd_match_ind++ )
            {
                DMatch forward = match_12[fwd_ind][fwd_match_ind];
                
				if(match_21[forward.trainIdx][0].distance < match_21[forward.trainIdx][1].distance)
                    ratio21 = match_21[forward.trainIdx][0].distance/match_21[forward.trainIdx][1].distance;
                
				if(match_21[forward.trainIdx][0].distance > match_21[forward.trainIdx][1].distance)
                    ratio21 = match_21[forward.trainIdx][1].distance/match_21[forward.trainIdx][0].distance;
                
				if(ratio21 <= thresh)
                {
                    for( size_t bkw_ind = 0; bkw_ind < match_21[forward.trainIdx].size(); bkw_ind++ )
                    {
                        DMatch backward = match_21[forward.trainIdx][bkw_ind];
                        
						// look for forward as well as backward matches
						if( backward.trainIdx == forward.queryIdx )
                        {
                            match_sym.push_back(forward);
                        }
                    }
                }
            }
        }
    }
}


void radiusMatch(const vector<vector<DMatch> > &match_12, vector<vector<DMatch> > &match_21, vector<DMatch> &match_sym, double thresh )
{

}


int findMatchingPoints(string path_img1, string path_img2, vector<vector<Point2f>> &img_pair_pts, double thresh1, int thresh2, bool show_img)
{
	Mat img1 = imread(path_img1,CV_LOAD_IMAGE_COLOR);
	Mat img2 = imread(path_img2,CV_LOAD_IMAGE_COLOR);
	
	//Mat img1, img2;

	//resize(img_1, img1, Size(0,0), 0.3, 0.3);
	//resize(img_2, img2, Size(0,0), 0.3, 0.3);

	string fname_img1, fname_img2, path_str1, path_str2;

	size_t dot = path_img1.find_last_of(".");
    
	size_t slash = path_img1.find_last_of("\\");
	if (slash != std::string::npos)
        path_str1 = path_img1.substr(0, slash);

	path_str1.append("\\Data\\");
	fname_img1 = path_str1;

	if (dot != std::string::npos)
        fname_img1.append(path_img1.substr(slash, dot-slash));

	dot = path_img2.find_last_of(".");
	slash = path_img2.find_last_of("\\");
	if (slash != std::string::npos)
        path_str2 = path_img2.substr(0, slash);
	
	path_str2.append("\\Data\\");
	fname_img2 = path_str2;

    if (dot != std::string::npos)
        fname_img2.append(path_img2.substr(slash, dot-slash));

	fname_img1.append(".yml");
	fname_img2.append(".yml");

	ifstream infile1(fname_img1);
	ifstream infile2(fname_img2);

	if(!infile1.good() || !infile2.good())
	{
		cout << "\ndescriptors files not found ... computing keypoints and descriptors\n";

		Mat gray_img1,gray_img2;

		cvtColor(img1, gray_img1, CV_RGB2GRAY);
		cvtColor(img2, gray_img2, CV_RGB2GRAY);
	
		SurfFeatureDetector detector;
		vector< KeyPoint > keypts1;
		vector< KeyPoint > keypts2;
		detector.detect(gray_img1, keypts1);

		if(keypts1.empty())
		{
			cout << "\nno keypoints found for image 1\n";
			
			return -5;
		}

		cout << "no of keypts-1: " << keypts1.size() << "\n";
		detector.detect(gray_img2, keypts2);

		if(keypts2.empty())
		{
			cout << "no keypoints found for image 2" << "\n";
			
			return -6;
		}
		cout << "no of keypts-2: " << keypts2.size() << "\n";

		Mat descriptors1, descriptors2;

		SiftDescriptorExtractor extractor;
		extractor.compute(gray_img1, keypts1, descriptors1);
		extractor.compute(gray_img2, keypts2, descriptors2);

		FileStorage fs_desc1(fname_img1, FileStorage::WRITE);
		write(fs_desc1, "descriptors", descriptors1);
		write(fs_desc1, "keypoints", keypts1);

		FileStorage fs_desc2(fname_img2, FileStorage::WRITE);
		write(fs_desc2, "descriptors", descriptors2);
		write(fs_desc2, "keypoints", keypts2);

		fs_desc1.release();
		fs_desc2.release();
	}

	FileStorage fs1(fname_img1, FileStorage::READ);
	if (!fs1.isOpened())
    { std::cout << "Failed to open " << fname_img1 << "\n"; return -7; }
	
	FileStorage fs2(fname_img2, FileStorage::READ);
	if (!fs2.isOpened())
    { std::cout << "Failed to open " << fname_img2 << "\n"; return -7; }

	// reading descriptors
	Mat descriptors1, descriptors2;
	fs1["descriptors"] >> descriptors1;
	fs2["descriptors"] >> descriptors2;

	// reading keypoints
	vector<KeyPoint> keypts1, keypts2;
	FileNode kyptNode1 = fs1["keypoints"]; 
	read(kyptNode1, keypts1);

	FileNode kyptNode2 = fs2["keypoints"]; 
	read(kyptNode2, keypts2);

	fs1.release();
	fs2.release();

	cout << "\nno of  kypts1: " << keypts1.size() << " , no of keypts2: " << keypts2.size() << "\n";

	vector<KeyPoint> kp1, kp2;
	Mat desc1, desc2;

	if(keypts1.size() < keypts2.size())  //img1 is training image, img2 is query image
	{
		kp1 = keypts2;
		descriptors1.copyTo(desc2);
		kp2 = keypts1;
		descriptors2.copyTo(desc1);
	}
	else
	{
		kp1 = keypts1;
		descriptors1.copyTo(desc1);
		kp2 = keypts2;
		descriptors2.copyTo(desc2);
	}

	vector<vector<DMatch> > match_12, match_21;
	vector< DMatch > good_matches;
 
	FlannBasedMatcher matcher;
	matcher.knnMatch(desc1, desc2, match_12, 2);

	//cout << "check surf 0.4 " << endl;

	matcher.knnMatch(desc2, desc1, match_21, 2);

	//cout << "check surf1" << endl;

	if(match_12.size()>0 && match_21.size()>0)
		CheckNNDR(match_12, match_21, good_matches, thresh1);
	
	if(good_matches.size() < thresh2)
	{
		cout << "only " << good_matches.size() << " matches found ... not enough" << std::endl;
		return 0;
	}

	cout << good_matches.size() << " good matches found ... " << std::endl;
	
	Mat img_matches;
	
	if(kp1.size() < kp2.size())
		drawMatches( img2, kp2, img1, kp1, good_matches, img_matches, Scalar::all(-1), Scalar::all(-1),
               vector<char>(), DrawMatchesFlags::NOT_DRAW_SINGLE_POINTS );
	else
		drawMatches( img1, kp1, img2, kp2, good_matches, img_matches, Scalar::all(-1), Scalar::all(-1),
               vector<char>(), DrawMatchesFlags::NOT_DRAW_SINGLE_POINTS );

	if(show_img)
	{
		imshow("matching features", img_matches);
		char key = waitKey(2);

		if(key == 'q')
			return 0;
	}
		
	vector< Point2f > _pt1;
	vector< Point2f > _pt2;

	for( int i = 0; i < good_matches.size(); i++ )
	{
		//-- Get the keypoints from the good matches
		_pt1.push_back( keypts1[ good_matches[i].queryIdx ].pt );
		_pt2.push_back( keypts2[ good_matches[i].trainIdx ].pt );
	}

	img_pair_pts.push_back(_pt1);
	img_pair_pts.push_back(_pt2);
	
	return 1;

}

void TakeSVDOfE(Mat_<double>& E, Mat& svd_u, Mat& svd_vt, Mat& svd_w) {
#if 1
	cout << "Using OpenCV's SVD" << endl;
	SVD svd(E,SVD::MODIFY_A);   //,SVD::MODIFY_A
	svd_u = svd.u;
	svd_vt = svd.vt;
	svd_w = svd.w;

	cout << "svd of E successful" << endl;

#else
	//Using Eigen's SVD
	cout << "Eigen3 SVD..\n";
	Eigen::Matrix3f  e = Eigen::Map<Eigen::Matrix<double,3,3,Eigen::RowMajor> >((double*)E.data).cast<float>();
	Eigen::JacobiSVD<Eigen::MatrixXf> svd(e, Eigen::ComputeThinU | Eigen::ComputeThinV);
	Eigen::MatrixXf Esvd_u = svd.matrixU();
	Eigen::MatrixXf Esvd_v = svd.matrixV();
	svd_u = (Mat_<double>(3,3) << Esvd_u(0,0), Esvd_u(0,1), Esvd_u(0,2),
						  Esvd_u(1,0), Esvd_u(1,1), Esvd_u(1,2), 
						  Esvd_u(2,0), Esvd_u(2,1), Esvd_u(2,2)); 
	Mat_<double> svd_v = (Mat_<double>(3,3) << Esvd_v(0,0), Esvd_v(0,1), Esvd_v(0,2),
						  Esvd_v(1,0), Esvd_v(1,1), Esvd_v(1,2), 
						  Esvd_v(2,0), Esvd_v(2,1), Esvd_v(2,2));
	svd_vt = svd_v.t();
	svd_w = (Mat_<double>(1,3) << svd.singularValues()[0] , svd.singularValues()[1] , svd.singularValues()[2]);
#endif
	
	/**
	cout << "----------------------- SVD ------------------------\n";
	cout << "U:\n"<<svd_u<<"\nW:\n"<<svd_w<<"\nVt:\n"<<svd_vt<<endl;
	cout << "----------------------------------------------------\n";
	**/
}


bool DecomposeEtoRandT(
	Mat_<double>& E,
	Mat_<double>& R1,
	Mat_<double>& R2,
	Mat_<double>& t1,
	Mat_<double>& t2) 
{
//#ifdef DECOMPOSE_SVD
	cout <<"Using HZ E decomposition\n";
	Mat svd_u, svd_vt, svd_w;
	TakeSVDOfE(E,svd_u,svd_vt,svd_w);

	cout << "check if first and second singular values are the same (as they should be)\n";
	double singular_values_ratio = fabsf(svd_w.at<double>(0) / svd_w.at<double>(1));
	if(singular_values_ratio>1.0) singular_values_ratio = 1.0/singular_values_ratio; // flip ratio to keep it [0,1]
	if (singular_values_ratio < 0.7) 
	{
		cout << "singular values are too far apart" << endl;
		return false;
	}

	Matx33d W(0,-1,0,	//HZ 9.13
		1,0,0,
		0,0,1);
	Matx33d Wt(0,1,0,
		-1,0,0,
		0,0,1);
	R1 = svd_u * Mat(W) * svd_vt; //HZ 9.19
	R2 = svd_u * Mat(Wt) * svd_vt; //HZ 9.19
	t1 = svd_u.col(2); //u3
	t2 = -svd_u.col(2); //u3
	return true;
	/**
#else
	//Using Horn E decomposition
	DecomposeEssentialUsingHorn90(E[0],R1[0],R2[0],t1[0],t2[0]);
#endif
	return true;
	**/
}


//************************************************************************************************************************************/
									//Find Camera Projection Matrix Using Fundamental Matrix Method//
//*************************************************************************************************************************************/



int findCamProjMatrix(Mat F, Mat K, vector<Matx34d> &P)
{
	Matx34d P_;
	Mat_<double> E = K.t() * F * K;

	//according to http://en.wikipedia.org/wiki/Essential_matrix#Properties_of_the_essential_matrix
	if(fabsf(determinant(E)) > 1e-07) 
	{
		cout << "det(E) != 0 : " << determinant(E) << "\n";
		P_ = 0;
		P.push_back(P_);
		return -1;
	}

	Mat_<double> R1(3,3);
	Mat_<double> R2(3,3);
	Mat_<double> t1(1,3);
	Mat_<double> t2(1,3);

	//decompose E to P' , HZ (9.19)
	{			
		if (!DecomposeEtoRandT(E, R1, R2, t1, t2)) 
		{
			cout << "E not decomposed into R & T successfully\n";
			P_ = 0;
			P.push_back(P_);
			return -1;
		}

		cout << "E decomposed into R & T successfully\n";

		vector<Mat_<double>> R, t;
		R.push_back(R1); R.push_back(R2); t.push_back(t1); t.push_back(t2);
		
		for(int i = 0; i < 2; i++)
		{
			if(determinant(R[i])+1.0 < 1e-09) 
			{
				//according to http://en.wikipedia.org/wiki/Essential_matrix#Showing_that_it_is_valid
				cout << "det(R) == -1 ["<<determinant(R1)<<"]: flip E's sign" << endl;
				E = -E;
				DecomposeEtoRandT(E, R1, R2, t1, t2);
				R.clear(); t.clear(); R.push_back(R1); R.push_back(R2); t.push_back(t1); t.push_back(t2);
				i = 0;
			}
			
			// check for coherance
			if (fabsf(determinant(R[i]))-1.0 > 1e-07) 
			{
				cout << "resulting rotation is not coherent\n";
				P_ = 0;
				int er1;
				cin >> er1;
				P.push_back(P_);
				return 0;
			}
		
			cout << "resulting rotation is coherent\n";
			for(int j = 0; j < 2; j++)
			{
				P_ = Matx34d(R[i](0,0),	R[i](0,1),	R[i](0,2),	t[j](0),
							 R[i](1,0),	R[i](1,1),	R[i](1,2),	t[j](1),
							 R[i](2,0),	R[i](2,1),	R[i](2,2),	t[j](2));
				P.push_back(P_);
			}
		}
	}

	//if(P==0)
		//cout << "\nno projection found for camera\n";

	return 1;
}


Matx34d findCanonicalCam(Mat F)
{
	Mat Ft = Mat::zeros(3, 3, CV_64FC1);
	Ft = F.t();

	Mat U = Mat::zeros(3, 3, CV_64FC1);
	Mat S = Mat::zeros(3, 3, CV_64FC1);
	Mat V = Mat::zeros(3, 3, CV_64FC1);

	SVD svd(Ft);

	S = Mat::diag(svd.w);
	U = svd.u;
	V = svd.vt.t();

	Mat e2 = Mat::zeros(3, 1, CV_64FC1);
	e2.col(0) = V.col(2);

	cout << "\n123...\n" << "\n";

	Mat e2x = Mat::zeros(3, 3, CV_64FC1);
	e2x.at<float>(0,1) = -e2.at<float>(2,0);
	e2x.at<float>(0,2) = e2.at<float>(1,0);
	e2x.at<float>(1,2) = -e2.at<float>(0,0);
	e2x.at<float>(1,0) = e2.at<float>(2,0);
	e2x.at<float>(2,0) = -e2.at<float>(1,0);
	e2x.at<float>(2,1) = e2.at<float>(0,0);

	Mat tmp33 = Mat::zeros(3, 3, CV_64FC1);

	gemm(e2x, F, 1, 0, 0, tmp33);

	cout << "\n gemm done .. \n" << "\n" << endl;

	Mat P_ = Mat::zeros(3, 4, CV_64FC1);

	for (int i = 0; i < 3; ++i) 
        for (int j = 0; j < 3; ++j) 
            P_.at<float>(i,j) = tmp33.at<float>(i,j); 

	P_.col(3) = e2.col(0);

	Matx34d P = P_;
	
	return P;
}


